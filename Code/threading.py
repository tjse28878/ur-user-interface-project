#from threading import Event, Thread

loop_start=[0]
class Flag(Event):
    def __bool__(self):
        return self.is_set()
        
class thread_1:
    def __init__(self, quit_flag, name=None, interval=1):
        if name is None:
            name=id(self)
        #hopefully this will make it quit
        self.quit_flag=quit_flag
        self.name=name
        self.interval=interval
        DHTPin = 17     #define the pin of DHT11
        self.dht = DHT.DHT(DHTPin)
        #print("Humidity : %.2f, \t Temperature : %.2f \n"%(dht.humidity,dht.temperature))        
        #self.dhtDevice = DHT11(board.D4)
        #self.temperature_c = self.dht.temperature
        #self.humidity = self.dht.humidity
    
    def run(self):
        try:
            while not self.quit_flag:
############### MODULE CODE COMES HERE ###############
                try:
                     # Print the values to the serial port
					self.chk = self.dht.readDHT11()
					temperature_c=self.dht.temperature
					temperature_f = temperature_c * (9 / 5) + 32
					humidity=self.dht.humidity
					print("Temp: {:.1f} F / {:.1f} C    Humidity: {}% "
                           .format(temperature_f, temperature_c, humidity))
                except RuntimeError as error:     # Errors happen fairly often, DHT's are hard to read, just keep going
					print(error.args[0])
                print('thread 1 running')
                module_1_return_value[0]=temperature_c
                #print(module_1_return_value[0])
                module_4_return_value[0]=humidity
                #print(module_4_return_value[0])
                sleep(self.interval)
############### MODULE CODE ENDS HERE  ###############
        finally:
########### CLEANUP STARTS HERE ###########
            print('cleanup stuff')
            

if __name__=='__main__':
    try:
        flag=Flag()
        # Create some tasks
        tasks=[thread_1(flag)]

        # Create some threads
        threads=[Thread(target=thread.run) for thread in tasks]
        
        #start the threads
        for thread in threads: thread.start()

        #do nothing while the threads do their thing
        while True:
            sleep(1)

    except KeyboardInterrupt:
        flag.set()
    finally:
        flag.set()

        while threads:
            for thread in threads:
                thread.join(0.1)
                if thread.is_alive():
                    print('waiting to join')
                else:
                    print('joining')
                    threads.remove(thread)
        print('program terminated')